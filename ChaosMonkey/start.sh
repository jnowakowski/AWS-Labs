#!/bin/bash

set -x
set -e
set -u
set -o pipefail

STACK_NAME="ChaosMonkey-$(date +%Y%m%d-%H%M%S)"
SSH_LOCATION="$(curl -s ipinfo.io/ip)/32"
KEY_NAME=${1:-awslabs}

aws cloudformation validate-template --template-body file://cm.yaml || exit -1
aws cloudformation create-stack \
  --template-body file://cm.yaml \
  --stack-name "${STACK_NAME}" \
  --capabilities CAPABILITY_IAM \
  --parameters ParameterKey=KeyName,ParameterValue="${KEY_NAME}" \
          ParameterKey=SSHLocation,ParameterValue="${SSH_LOCATION}"

aws cloudformation wait stack-create-complete \
    --stack-name "${STACK_NAME}" \
    --region eu-west-1

aws cloudformation describe-stacks \
    --region eu-west-1 \
    --stack-name "${STACK_NAME}"


