#!/bin/bash
yum -y update
yum -y install jq install java-1.8.0-openjdk java-1.8.0-openjdk-devel
yum remove java-1.7.0-openjdk
curl https://storage.googleapis.com/golang/go1.8.1.linux-amd64.tar.gz -o /tmp/go.tar.gz
export PATH=$PATH:/usr/local/go/bin
tar -C /usr/local -xzf /tmp/go.tar.gz
export GOROOT=/usr/local/go
export GOBIN=$GOROOT/bin
mkdir /tmp/golang/
export GOPATH=/tmp/golang/ 
curl -L https://services.gradle.org/distributions/gradle-3.5-bin.zip -o /tmp/gradle.zip
mkdir /opt/gradle
unzip -d /opt/gradle /tmp/gradle.zip
export PATH=$PATH:/opt/gradle/gradle-3.5/bin
echo "export PATH=$PATH" >> /home/ec2-user/.bash_profile
echo "export GOROOT=/usr/local/go" >> /home/ec2-user/.bash_profile
echo "export GOBIN=$GOROOT/bin" >> /home/ec2-user/.bash_profile
echo "export GOPATH=/tmp/golang/" >> /home/ec2-user/.bash_profile
(
cd /home/ec2-user
git clone git://github.com/Netflix/SimianArmy.git
cd SimianArmy
./gradlew build
)
CM_HOME=/home/ec2-user/SimianArmy
CONFIG_DIR="${CM_HOME}/src/main/resources/"
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq .region -r)
sed -i "s/^simianarmy.client.aws.region.*/simianarmy.client.aws.region = $REGION/" ${CONFIG_DIR}/client.properties
sed -i "s/^simianarmy.chaos.leashed.*/simianarmy.chaos.leashed = false/" ${CONFIG_DIR}/chaos.properties
sed -i "s/^simianarmy.chaos.ASG.enabled.*/simianarmy.chaos.ASG.enabled = true/" ${CONFIG_DIR}/chaos.properties
sed -i "s/^simianarmy.janitor.enabled.*/simianarmy.janitor.enabled = false/" ${CONFIG_DIR}/janitor.properties
chown -R ec2-user:ec2-user ${CM_HOME}
