#!/bin/bash
sudo yum -y update
sudo yum install -y ruby wget
wget https://aws-codedeploy-eu-west-1.s3.amazonaws.com/latest/install
chmod +x ./install
sudo ./install auto
sudo service codedeploy-agent status
sudo yum install -y tomcat8-webapps tomcat8-admin-webapps
sudo chkconfig tomcat8 on
sudo service tomcat8 start
