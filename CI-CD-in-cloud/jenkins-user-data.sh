#!/bin/bash
sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key
sudo yum update -y
sudo yum upgrade -y
sudo yum install -y java-1.8.0 java-1.8.0-devel git
sudo yum remove -y java-1.7.0-openjdk
sudo wget https://services.gradle.org/distributions/gradle-3.4-bin.zip -O /opt/gradle.zip
sudo unzip /opt/gradle.zip -d /opt/
sudo yum install -y jenkins
echo PATH=\$PATH:/opt/gradle-3.4/bin/ |sudo tee -a /etc/sysconfig/jenkins
sudo service jenkins start
sudo chkconfig jenkins on

sleep 60
sudo chsh -s /bin/bash jenkins
echo "http://$(curl -s http://169.254.169.254/latest/meta-data/public-ipv4):8080/"
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
