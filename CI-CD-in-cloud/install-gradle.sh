#!/bin/bash
sudo wget https://services.gradle.org/distributions/gradle-3.4-bin.zip -O /opt/gradle.zip
sudo unzip /opt/gradle.zip -d /opt/
echo PATH=\$PATH:/opt/gradle-3.4/bin/ |sudo tee -a /etc/sysconfig/jenkins
sudo service jenkins restart
