#!/bin/bash
aws cloudformation create-stack \
    --region eu-west-1 \
    --stack-name workers-022 \
    --template-body file://workers.yaml \
    --tags Key=BuildUrl,Value="${BUILD_URL}" \
    --parameters \
       ParameterKey=WorkerUserData,ParameterValue="$(base64 -w0 install-codedeploy-agent.sh)" \
       ParameterKey=BuildUrl,ParameterValue="${BUILD_URL}"

aws cloudformation wait stack-create-complete --stack-name workers-022 --region eu-west-1
