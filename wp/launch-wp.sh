#!/bin/bash

set -e
set -u
set -o pipefail
set -x

aws cloudformation validate-template --template-body file://WordPress_Single_Instance.json

aws cloudformation create-stack \
  --region eu-west-1 \
  --stack-name wp \
  --template-body file://WordPress_Single_Instance.json \
  --parameters \
      ParameterKey=KeyName,ParameterValue="aws-labs-3" \
      ParameterKey=SSHLocation,ParameterValue="0.0.0.0/0" \
      ParameterKey=DBPassword,ParameterValue="DBPassword" \
      ParameterKey=DBName,ParameterValue="wordpressdb" \
      ParameterKey=DBUser,ParameterValue="DBUser" \
      ParameterKey=DBRootPassword,ParameterValue="DBRootPassword" \
      ParameterKey=InstanceType,ParameterValue="t2.small"

aws cloudformation wait stack-create-complete --stack-name wp --region eu-west-1

aws cloudformation describe-stacks --stack-name wp --region eu-west-1
