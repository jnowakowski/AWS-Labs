#!/bin/bash

set -e
set -u
set -o pipefail
set -x

aws cloudformation validate-template --template-body file://launch-demo-parameter-store-access.yaml
STACK_NAME=demo-parameter-store-access-${RANDOM}

aws cloudformation create-stack \
    --region eu-west-1 \
    --stack-name ${STACK_NAME} \
    --template-body file://launch-demo-parameter-store-access.yaml \
    --tags Key=description,Value="demo-parameter-store-access"hhh

aws cloudformation wait stack-create-complete --stack-name ${STACK_NAME} --region eu-west-1
