# Kontakt:
* Janusz Nowakowski <jnowakowski@gmail.com>  
* +48 724 33 11 98

# PuTTY download:
* http://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
* https://the.earth.li/~sgtatham/putty/latest/w64/putty.exe
* https://the.earth.li/~sgtatham/putty/latest/w64/puttygen.exe  
