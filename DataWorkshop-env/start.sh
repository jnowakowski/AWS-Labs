#!/bin/bash

set -e
set -u
set -o pipefail

STACK_NAME="DataWorkshop-$(date +%Y%m%d-%H%M%S)"

if [ $# -ne 0 ]; then
  echo "Starting instance with SSH access"
  echo "SSH KeyPair name: ${1}"
  aws cloudformation validate-template --template-body file://DataWorkshop-env.yaml || exit -1
  aws cloudformation create-stack \
    --template-body file://DataWorkshop-env.yaml \
    --stack-name "${STACK_NAME}" \
    --parameters ParameterKey=SSHKey,ParameterValue="${1}"
else
  echo "Staring instance without SSH access"
  aws cloudformation validate-template --template-body file://DataWorkshop-env-nossh.yaml || exit -1
  aws cloudformation create-stack \
    --template-body file://DataWorkshop-env-nossh.yaml \
    --stack-name "${STACK_NAME}" \
    --on-failure DELETE
fi

aws cloudformation wait stack-create-complete \
    --stack-name "${STACK_NAME}" \
    --region eu-west-1

aws cloudformation describe-stacks \
    --region eu-west-1 \
    --stack-name "${STACK_NAME}"
